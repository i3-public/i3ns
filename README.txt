
# wget -qO- https://gitlab.com/i3-public/i3ns/-/raw/main/README.txt | bash
# domains: nscloudflare.net, i3ns.net, enzoferrari.online
# https://www.name.com/account/domain/details/nscloudflare.net#nsregistration
# free-up port 53 : https://t.ly/19YHD

docker rm -f i3ns
docker rmi i3ns-image

wget -O dockerfile https://gitlab.com/i3-public/i3ns/-/raw/main/conf/dockerfile
docker build -t i3ns-image -f dockerfile .
docker run -t -d --restart unless-stopped --hostname i3ns --name i3ns -p 53:53/tcp -p 53:53/udp -p 8053:8053 i3ns-image

